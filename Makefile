#
#  To sa opcje dla kompilacji
#
CPPFLAGS= -c -g -Iinc -Wall -pedantic

__start__: szablonur
	./szablonur

szablonur: obj/main.o 
	g++ -Wall -pedantic -o system_of_equations obj/main.o 

obj/main.o: src/main.cpp  src/LZespolona.cpp inc/SMatrix.hh inc/SVector.hh\
        inc/Size.hh inc/LZespolona.hh inc/SSystem.hh
	g++ ${CPPFLAGS} -o obj/main.o src/main.cpp



clean:
	rm -f obj/*.o szablonur