#include "Size.hh"
#include "LZespolona.hh"
#include <iostream>

template <typename SType, int SSize>
class SVector
{
    SType vx [SSize];
    public:

    SType  operator [] (unsigned int idx) const { return vx[idx]; }
    SType &operator [] (unsigned int idx)       { return vx[idx]; }

    SVector<SType,SSize> operator - (const SVector<SType,SSize> &subs) const;
    SVector<SType,SSize> operator + (const SVector<SType,SSize> &add) const;
    SType operator * (const SVector<SType,SSize> &scalar) const;
    SVector<SType,SSize> operator * (double mult) const; 
    SVector<SType,SSize> operator * (LZespolona mult) const; //mnożenie przez LZespolone
};

template <typename SType, int SSize>
std::ostream& operator << (std::ostream &os, const SVector<SType, SSize>& V)
{
    for (unsigned int x = 0; x < SSize; x++)
    {
        os << V[x] << " ";
    }
    return os;
}
template <typename SType, int SSize>
std::istream &operator >>(std::istream &stream, SVector<SType, SSize> &vec)
{
for(int x = 0; x<SSize; x++)
{
  stream >> vec[x];
  while (isspace(stream.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        stream.ignore();
    }
}
  return stream;
}

template <typename SType, int SSize>
SVector<SType, SSize> SVector<SType, SSize>::operator * (double mult) const
{
    SVector <SType, SSize> result;
    for (unsigned int x = 0; x < SSize; x++)
    {
        result [x] = (*this)[x] * mult;
    }
    return result;

}
template <typename SType, int SSize>
SVector<SType,SSize> SVector<SType, SSize>::operator * (LZespolona mult) const
{
    SVector <SType, SSize> result;
    for (unsigned int x = 0; x < SSize; x++)
    {
        result [x] = (*this)[x] * mult;
    }
    return result;

}

template <typename SType, int SSize>
SVector<SType, SSize> SVector<SType, SSize>::operator - (const SVector<SType, SSize> &subs) const
{
    SVector <SType, SSize> result;
    for (unsigned int x = 0; x < SSize; x++)
    {
        result [x] = (*this)[x] - subs[x];
    }
    return result;
}

template <typename SType, int SSize>
SType SVector<SType, SSize>::operator * (const SVector<SType, SSize> &scalar) const
{
    SType result;
    result = 0;
    SType temp;   //zmienna tymczasowa, przechowuje dotychczasowe obliczenia
    
  
    for(unsigned int x = 0; x< SSize; x++)                 
    {
        temp = (*this)[x] * scalar[x];
        result = result + temp;
    }

  return result;
}

template <typename SType, int SSize>
SVector<SType, SSize> SVector<SType, SSize>::operator + (const SVector<SType, SSize> &add) const
{
    SVector <SType, SSize> result;
    for (unsigned int x = 0; x < SSize; x++)
    {
        result [x] = (*this)[x] + add[x];
    }
    return result;
}