#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>


/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.
 */


/*!
 * Modeluje pojecie liczby zespolonej
 */
struct  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */
  LZespolona &operator = (double  number);
  
};


/*
 * Dalej powinny pojawic sie zapowiedzi definicji przeciazen operatorow
 */

std::ostream &operator << (std::ostream &os, LZespolona  Zesp);
std::istream& operator >> (std::istream& is, LZespolona& LZ);

LZespolona operator + (LZespolona Skl1,  LZespolona Skl2);
LZespolona operator - (LZespolona Skl1, LZespolona Skl2);
LZespolona operator * (LZespolona Skl1, LZespolona Skl2);
LZespolona operator * (LZespolona Skl1, double mult);
LZespolona operator / (LZespolona Skl1, LZespolona Skl2);

bool operator == (LZespolona LZ, double x);
double abs(const LZespolona &Z);
double sqrt(const LZespolona &Z);

/* MODYFIKACJA */
LZespolona operator *= (LZespolona &Skl1, LZespolona Skl2);
LZespolona operator += (LZespolona &Skl1, LZespolona Skl2);
double Arg(LZespolona Z1);
double modul(const LZespolona &Z);



#endif
