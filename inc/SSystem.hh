#pragma once

#include "SMatrix.hh"
#include <fstream>
#include <iostream>
#include <cmath>
#include <iomanip>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename SType, int SSize>
class SSystem
{
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */

  
  template <typename S, int Size> friend std::istream &operator>>(std::istream &stream, SSystem <S, Size> &system);
  template <typename S, int Size> friend std::ostream &operator<<(std::ostream &stream, SSystem <S, Size> &system);
  SMatrix<SType, SSize> matrix;
  SVector<SType, SSize> vector; //wyrazy wolne
public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
const SVector<SType, SSize>& operator[ ] (int size) const { return matrix[size]; }
SVector<SType, SSize>& operator[ ] (int size) { return matrix[size]; }
SType term (int idx) const {return vector[idx];}
SType& term (int idx) { return vector[idx]; }
SMatrix<SType, SSize> trans () {return matrix.tran();}



SType sdet() const;

SType solve ();

SSystem<SType, SSize> operator = (SMatrix <SType, SSize>  matrx);   // podstawia macierz do systemu
};

template <typename SType, int SSize>
std::ostream &operator<<(std::ostream &stream, SSystem <SType, SSize> &system)
{
    for (int x = 0; x<SSize; x++)
    {
        if (x == (SSize/2 - 0.5) || x == SSize/2)
        {
            stream << "|" << system.matrix[x] << "| " << "|x_" << x+1 << "| = " << "|" << system.vector[x] << "|" << std::endl;
        }
        else
        {
            stream << "|" << system.matrix[x] << "| " << "|x_" << x+1 << "|   " << "|" << system.vector[x] << "|" << std::endl;
        }
    }
    return stream;
}

template <typename SType, int SSize>
std::istream &operator>>(std::istream &stream, SSystem <SType, SSize> &system)
{
    for (int x =0; x<SSize; x++)
    {
        stream >> system.matrix[x];
    }
    stream >> system.vector;

    return stream;
}


template <typename SType, int SSize>
SSystem<SType, SSize> SSystem<SType, SSize>::operator = (SMatrix <SType, SSize>  matrx)
{
    (*this).matrix = matrx;

    return *this;
}

template <typename SType, int SSize>
SType SSystem<SType, SSize>::sdet() const
{   
    SSystem <SType, SSize> temp = (*this); // zmienna pomocnicza
    // jezeli pierwszy element zerowy, dodaj nastepny niezerowy z wierszy nizej
    for (int x = 0; x < SSize; x++)
    {
        if (abs(temp.matrix[x][x]) < EPS)
        {
            for (int k = 0; k < SSize; k++)
            {
                if (abs(temp.matrix[k][x]) >= EPS)
                {
                    temp.matrix[x] = temp.matrix[x] + temp.matrix[k];
                    temp.vector[x] = temp.vector[x] + temp.vector[k];
                    break;
                }
            }
        }
    }

    // zerowanie wierszy nizej, tworzenie macierzy trojkatnej
    for (int c = 0; c < SSize; c++)
    {
       
        
            for (int r = c + 1 ; r < SSize; r++)
            {
                SType factor = temp.matrix[r][c] / temp.matrix[c][c];
                temp.matrix[r] = temp.matrix[r] - (temp.matrix[c] * factor);
                temp.vector[r] = temp.vector[r] - temp.vector[c] * factor;

     
            }
        
    }
 
    // liczenie wyznacznika macierzy
    SType det;
    det = temp.matrix[0][0];
    for (int i = 1; i < SSize; i++)
    {
        det = det * temp.matrix[i][i];
    }

    return det;
}


template <typename SType, int SSize>
SType SSystem<SType, SSize>::solve ()
{
    SVector<SType, SSize> result, error; // tablica niewiadomych i bledu
    SSystem <SType, SSize> sys, systemp; //zmienna tymczasowa, przechowuje kopie
    SType det, dettemp; //zmienna wyznacznik oraz tymczasowa do wyliczania
    sys = (*this);
    det = sys.sdet();

    std::cout << "Rozwiazania ukladu rownan: " <<std::endl;
    if (det == 0)
    {
        std::cout << "Wyznacznik rowny 0, brak rozwiazan, nieskonczona ilosc";
    }
    else
    {
    for (int x = 0; x<SSize; x++)
    {
        systemp = (*this);
        systemp = systemp.trans();  //transponuje, żeby przekształcić do wymaganej postaci
        systemp.matrix[x] = systemp.vector;
        
        dettemp = systemp.sdet();
        
        result[x] = dettemp/det;

        std::cout << std::fixed << std::setprecision(2) << "x_" << x+1 << " = " << result[x] << "  ";
       
    }
    systemp = (*this);
    error = systemp.matrix*result - systemp.vector;
        std::cout << std::endl;
        std::cout << std::scientific << std::setprecision(1)<< "Wektor bledu:             Ax-b   = (" << error  << ")" << std::endl;
        
        std::cout << "Dlugosc wektora bledu : ||Ax-b|| = "<< sqrt(error*error);
    }
    return det;
}