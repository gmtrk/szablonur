#pragma once

#include "Size.hh"
#include "SVector.hh"
#include <iostream>

/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename SType, int SSize>
class SMatrix
{
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  SVector<SType,SSize> Row[SSize];
public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */

SVector<SType, SSize> operator[ ] (int size) const { return Row[size]; }
SVector<SType, SSize>& operator[ ] (int size) { return Row[size]; }

SMatrix <SType, SSize> operator * (const SMatrix <SType, SSize>& m) const; //macierz przez macierz
SVector <SType, SSize> operator * (SVector<SType, SSize> &Vec) const; // mnożenie macierzy przez wektor

SMatrix <SType, SSize> tran() const;

SMatrix <SType, SSize> operator = (const LZespolona &LZ); // przypisanie LZespolona do Matrix 
bool operator == (const SMatrix <SType, SSize>& m) const;

SType det() const; // det dla macierzy

};



/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
template <typename SType, int SSize>
std::ostream &operator<<(std::ostream &stream, const SMatrix <SType, SSize> &matrix)
{
    for(int x = 0; x<SSize; x++)
    {
        stream << matrix[x] << std::endl;
        
    }
    return stream;
}
template <typename SType, int SSize>
SVector <SType, SSize> SMatrix<SType, SSize>::operator * (SVector<SType, SSize> &Vec) const  //mnozenie macierzy przez wektor
{
    SVector<SType, SSize> result;
    for (int x = 0; x <SSize ; x++)
    {
        result[x] = 0;             //inicjalizacja
    }
    for(int x = 0; x<SSize; x++)
    {
        for(int y = 0; y<SSize; y++)
        {
           result[x] = result[x] + (*this)[x][y] * Vec[y];
        }
    }
    return result;
}


template <typename SType, int SSize>
SMatrix<SType, SSize> SMatrix<SType, SSize>::tran() const //macierz transponowana
{
    SMatrix<SType, SSize> trans;
    for (int x = 0; x<SSize ; x++)
    {
        for (int y = 0; y<SSize ; y++)
        {
            trans[y][x] = (*this)[x][y];
        }
    }
   return trans;

}
template <typename SType, int SSize>
SMatrix<SType, SSize> SMatrix<SType, SSize>::operator * (const SMatrix<SType, SSize>& m) const
{
    SMatrix<SType, SSize> result;

    for(int x = 0; x<SSize; x++) // inicjalizacja
    {
        for(int y = 0; y<SSize; y++)
        {
            result[x][y] = 0;
        }
    }

  
    for (int x = 0; x<SSize; x++)
    {
        for (int y = 0; y<SSize; y++)
        {
            for (int k = 0; k<SSize; k++)
            {
                result[x][y] = (*this)[x][k] * m[k][y] + result[x][y];
            }
        }
    }
    return result;
}
template <typename SType, int SSize>
SMatrix <SType, SSize> SMatrix<SType, SSize>::operator = (const LZespolona &LZ)
{
    if (SSize == 2)
    {
        (*this)[0][0] = LZ.re; (*this)[0][1] = -LZ.im; (*this)[1][0] = LZ.im; (*this)[1][1] = LZ.re;
    }
    if (SSize == 4)
    {   
        for (int x = 0; x<SSize; x++)
        {
            for (int y = 0; y<SSize; y++)
            {
                (*this)[x][y] = 0; //inichaliacja
            }
        }
        for (int x = 0; x<SSize; x++)
        {
            (*this)[x][x] = LZ.re;
            (*this)[x][SSize - x - 1] = LZ.im;
            if (x > 1)
            {
                (*this)[x][SSize - x - 1] = -LZ.im;
            }
        }
    }
    return (*this);
}
template <typename SType, int SSize>
bool SMatrix<SType,SSize>::operator == (const SMatrix <SType, SSize>& m) const
{
    int istrue = 0;
    for(int x = 0; x<SSize; x++)
    {
        for(int y = 0; y<SSize; y++)
        {
            if((*this)[x][y] != m[x][y])
            {
                istrue = 1;
            }
        }
    }

    if(istrue == 0)
    {return true;}
    else
    {return false;}
    
}

template <typename SType, int SSize>
SType SMatrix<SType, SSize>::det() const
{   
    SMatrix <SType, SSize> temp = (*this); // zmienna pomocnicza
    SType det;
    
   
    
    // jezeli pierwszy element zerowy, dodaj nastepny niezerowy z wierszy nizej
    for (int x = 0; x < SSize; x++)
    {
        if (abs(temp[x][x]) < EPS)
        {
            for (int k = 0; k < SSize; k++)
            {
                if (abs(temp[k][x]) >= EPS)
                {
                    temp[x] = temp[x] + temp[k];
                    break;
                }
            }
        }
    }

    // zerowanie wierszy nizej, tworzenie macierzy trojkatnej
    for (int c = 0; c < SSize; c++)
    {
       
        
            for (int r = c + 1 ; r < SSize; r++)
            {
                SType factor = temp[r][c] / temp[c][c];
                temp[r] = temp[r] - (temp[c] * factor);


     
            }
        
    }
 
    // liczenie wyznacznika macierzy
    
    det = temp[0][0];
    for (int i = 1; i < SSize; i++)
    {
        det = det * temp[i][i];
    }
 
    
    return det;
}