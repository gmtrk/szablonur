#include "LZespolona.hh"
#include "SSystem.hh"
#include <string>


using namespace std;

void TestCtoM()
{
     SMatrix<double, 2> complex2, complex22, result2;
     SMatrix<double, 4> complex4, complex44, result4;
     LZespolona zespolona1, zespolona2;
     zespolona1.re = 3; zespolona1.im = 2;  zespolona2.re = 1; zespolona2.im = 5;
     complex2 = zespolona1; complex22 = zespolona2; complex4 = zespolona2; complex44 = zespolona1;
     cout << "Twoja liczby zespolone :" << zespolona1 << "   " << zespolona2 <<endl;
     cout << "Twoja macierz 2x2 z 1 liczby:" << endl << complex2 << endl;
     cout << "Twoja macierz 4x4 z 2 liczby:" << endl << complex4 << endl;
     result2 = zespolona1 * zespolona2;
     result4 = zespolona1 * zespolona2;
     cout << "Porownanie iloczynu liczb z iloczynem ich macierzy2x2" << endl;
     if (result2 == complex2 * complex22)
     {
          cout << "Liczby sa takie same" << endl;
     }
     else
     {
          cout << "Liczby nie sa takie same" << endl;
     }
     cout << "Porownanie iloczynu liczb z iloczynem ich macierzy4x4" << endl;
     if (result4 == complex4 * complex44)
     {
          cout << "Liczby sa takie same" << endl;
     }
     else
     {
          cout << "Liczby nie sa takie same" << endl;
     }
     cout << "Porownanie det macierzy 2x2 z jej LZespoloną" << endl;
     if(complex2.det() == modul(zespolona1))
     {
          cout << "Sa rowne" << endl;
          cout << "Det macierzy:" << complex2.det() << "Modul LZespolonej" << modul(zespolona1) << endl;
     }
     else
     {
         cout << "Nie sa rowne" << endl;
          cout << "Det macierzy:" << complex2.det() << "Modul LZespolonej" << modul(zespolona1) << endl;
           cout << "Mozemy zauwazyc ze det macierzy to modul podniesiony do potegi 2" << endl;
     }
     cout << "Porownanie det macierzy 4x4 z jej LZespoloną" << endl;
     if(complex4.det() == modul(zespolona2))
     {
          cout << "Sa rowne" << endl;
           cout << "Det macierzy:" << complex4.det() << "Modul LZespolonej" << modul(zespolona2) << endl;;

     }
     else
     {
         cout << "Nie sa rowne" << endl;
          cout << "Det macierzy:" << complex4.det() << "Modul LZespolonej" << modul(zespolona2) << endl;;
          cout << "Mozemy zauwazyc ze det macierzy to modul podniesiony do potegi 4" << endl;
     }
     
     
}



int main()
{
 ifstream file;
 string name, test;
 
   cout << endl
       << " Start programu " << endl
       << endl;

cout << "Podaj nazwe pliku w formacie 'nazwa.txt', wpisz fraze: 'test'  , aby wlaczyc test modyfikacji" << endl;
cin >> name;
if (name == "test")
{
     TestCtoM();
     
}

file.open(name.c_str());
if(!file.good() && name != "test"){cerr<< "Nie udalo sie otworzyc pliku..."; return false;}
getline(file, test);

if (test == "r")   // dla rzeczywistych
{
     SSystem <double, SIZE> sys;

     file >> sys;
     sys = sys.trans();
     cout << "Twoje rownanie: " << endl;
     cout << sys;
     sys.solve();
}
else if (test == "z")
{
     SSystem <LZespolona, SIZE> sys;

     file >> sys;
     sys = sys.trans();
     cout << "Twoje rownanie: " << endl;
     cout << sys;
     sys.solve();
     


}
else if (name == "test"){cout << "Koniec programu";}
else
{
     cerr << "Nie mozna rozpoznac typu testu, dodaj na poczatku 'r' lub 'z' ";
}
}



/****************************************************************************************************
 * Jakub Gmiterek PO Pn 17:05 Układy równań liniowych                                               *
 * Program kompiluje się bez problemów.                                                             *
 * Przy starcie prosi mnie o podanie nazwy pliku z danymi,                                          *
 * Najpierw podaje rownan32ie.txt, jest  to plik który nie istnieje, więc program zwraca błąd i zaczynam go od nowa                          *
 * Następnie podaje rownanier.txt, program działa poprawnie dla liczb rzeczywistych                 *
 * Zmieniam dane, wpisując w losowe miejsca zera, sprawdzam czy algorytm będzie działał poprawnie.  *
 * Następnie wpisuje rownaniez.txt, program poprawnie liczy wyniki układu równań l zespolonych      *
 * Zmieniam losowo dane, sprawdzając wyniki z zewnętrznym kalkulatorem równań                       *
 * Uważam że program działa dobrze i spełnia wymagania zadania                                      *
 ****************************************************************************************************/