#include "LZespolona.hh"
#include <iostream>
#include <math.h>
#include <string>
#include <cmath>


#define PI 3.14159265



/*!
 * Podstawia do liczby zespolonej liczbę rzeczywistą.
 * Powoduje to, że w części urojonej zostanie automatycznie podstawiona
 * wartość 0.
 */
LZespolona &LZespolona::operator = (double  number)
{
  this->re = number; this->im = 0;
  return *this;
}

double abs(const LZespolona &Z)        // Do wyznaczania wyznacznika, sprawdza że zespolona jest wystarczająco bliska zeru
{
  double result;

  result = sqrt(Z.re*Z.re + Z.im*Z.im);

  return result;
}
bool operator == (LZespolona LZ, double x)  // porównanie Zespoloej do rzeczywistej
{
  if (LZ.re == x && LZ.im == 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}
double sqrt(const LZespolona &Z)   // pierwiastek
{
  double result;
  result = sqrt(Z.re*Z.re + Z.im*Z.im);
  return result;

}

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
/*!
 * Realizuje odejmowanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik odejmowania,
 *    Skl2 - drugi skladnik odejmnowania.
 * Zwraca:
 *    Różnice dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator - (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  return Wynik;
}
/*!
 * Realizuje mnożenie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik mnożenia,
 *    Skl2 - drugi skladnik mnożenia.
 * Zwraca:
 *    Iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator * (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;

  Wynik.re = (Skl1.re * Skl2.re) - (Skl1.im * Skl2.im);
  Wynik.im = (Skl1.im * Skl2.re) + (Skl2.im * Skl1.re);
  return Wynik;
}
/*!
 * Realizuje dzielenie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dzielenia,
 *    Skl2 - drugi skladnik dzielenia.
 * Argument Skl2.im jest ujemny co wynika z twierdzenia sprzężenia:
 * Sprzężenie z Skl1.re + Skl1.im = Skl1.re - Skl1.im
 * Zwraca:
 *    Iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator / (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;
  double mian = (Skl2.re*Skl2.re + Skl2.im*Skl2.im);
if (mian != 0){
  Wynik.re = ((Skl1.re * Skl2.re) - (Skl1.im * -Skl2.im))/mian;
  Wynik.im = ((Skl1.im * Skl2.re) + (-Skl2.im * Skl1.re))/mian;
  return Wynik;
}
else
  {
     std::cout << "Blad, dzielenie przez zero" << std::endl;
     return Skl1;
   
  }
}


LZespolona operator * (LZespolona Skl1, double mult)
{
  LZespolona result;
  result.re = Skl1.re * mult;
  result.im = Skl1.im * mult;

  return result;
}

std::ostream &operator << (std::ostream &os, LZespolona  Z1)
{

  os << '(' << Z1.re << std::showpos << Z1.im << "i)" << std::noshowpos;

  return os;
  
}
std::istream& operator >> (std::istream& is, LZespolona& LZ)
{
    while (isspace(is.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        is.ignore();
    }

    if (is.peek() != '(') //sprawdza czy pierwszy znak to nawias, jeżeli nie przechodzimy do failstate
    {
        is.setstate(std::ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' nawias
    is >> LZ.re; 
    is >> LZ.im;

    if (is.peek() != 'i') // sprawdza czy kolejny znak to i
    {
        is.setstate(std::ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' i
    if (is.peek() != ')')  // sprawdza czy ostatnia to nawias
    {
        is.setstate(std::ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' nawias
    return is;
}

/* MODYFIKACJA */

LZespolona operator += (LZespolona &Skl1, LZespolona Skl2)
{
  Skl1 = Skl1 + Skl2;
  return Skl1;
}

LZespolona operator *= (LZespolona &Skl1, LZespolona Skl2)
{
  Skl1 = Skl1 * Skl2;
  return Skl1;
}

double Arg(LZespolona Z1)
{
  double WynikR;  // wynik w radianach
  double Wynik; //wynik w stopniach
  if (Z1.re !=0)
  {
    if (Z1.re > 0)
    {
     WynikR = atan2(Z1.re, Z1.im);
    }
    else
    {
      WynikR = atan2(Z1.re, Z1.im) + PI;
    }
  }
  else if (Z1.im > 0)
  {
    WynikR = PI/2;
  }
  else if (Z1.im < 0)
  {
    WynikR = -PI/2;
  }

  
  if (Z1.re != 0 && Z1.im != 0)
  {
    Wynik = (WynikR * 180) / PI;  // konwersja radianów na stopnie
  }
  else
  {
     std::cout << "Brak wartości dla liczby (0+0i)" << std::endl;
  }
  
  return Wynik;
}


double modul(const LZespolona &Z)
{ double result =sqrt(Z.re*Z.re + Z.im*Z.im);
   return result;
}
